#version 400 core

in vec4 passedColor;
in vec2 passedUVCoords;

out vec4 outColor;

void main(void)
{
    outColor = passedColor;
}