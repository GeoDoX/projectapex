#version 400 core

in vec4 passedColor;
in vec2 passedUVCoords;

out vec4 outColor;

uniform sampler2D textureSampler;

void main(void)
{
	vec4 textureColor = texture(textureSampler, passedUvCoords);

    outColor = passedColor * textureColor;
}