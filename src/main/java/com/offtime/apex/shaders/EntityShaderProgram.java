package com.offtime.apex.shaders;

import com.offtime.apex.engine.files.Files;
import com.offtime.apex.engine.graphics.FragmentShader;
import com.offtime.apex.engine.graphics.ShaderProgram;
import com.offtime.apex.engine.graphics.VertexShader;

public class EntityShaderProgram extends ShaderProgram
{
    public EntityShaderProgram()
    {
        super
        (
            new VertexShader(Files.file(Files.WORKING_DIRECTORY_PATH, "res", "shaders", "entity", "VertexShader.glsl")),
            new FragmentShader(Files.file(Files.WORKING_DIRECTORY_PATH, "res", "shaders", "entity", "FragmentShader.glsl"))
        );
    }
}
