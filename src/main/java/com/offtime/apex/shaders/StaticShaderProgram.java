package com.offtime.apex.shaders;

import com.offtime.apex.engine.files.Files;
import com.offtime.apex.engine.graphics.FragmentShader;
import com.offtime.apex.engine.graphics.ShaderProgram;
import com.offtime.apex.engine.graphics.VertexShader;

public class StaticShaderProgram extends ShaderProgram
{
    public StaticShaderProgram()
    {
        super
        (
            new VertexShader(Files.file(Files.WORKING_DIRECTORY_PATH, "res", "shaders", "static", "VertexShader.glsl")),
            new FragmentShader(Files.file(Files.WORKING_DIRECTORY_PATH, "res", "shaders", "static", "FragmentShader.glsl"))
        );
    }

    @Override
    public void bindAttributeLocations()
    {
        bindAttribute(1, "position");
        bindAttribute(2, "color");
    }
}
