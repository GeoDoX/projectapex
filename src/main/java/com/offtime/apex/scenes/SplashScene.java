package com.offtime.apex.scenes;

import com.offtime.apex.engine.graphics.Mesh;
import com.offtime.apex.engine.graphics.ShaderProgram;
import com.offtime.apex.engine.input.Key;
import com.offtime.apex.engine.input.Keyboard;
import com.offtime.apex.engine.scene.Scene;
import com.offtime.apex.shaders.StaticShaderProgram;
import org.lwjgl.opengl.GL11;

public class SplashScene extends Scene
{
    ShaderProgram shader = new StaticShaderProgram();
    Mesh mesh;

    float[] vertices =
    {
        -0.5f, -0.5f, 0f,
        -0.5f, 0.5f, 0f,
        0.5f, 0.5f, 0f,
        0.5f, -0.5f, 0f,

        0.5f, -0.5f, 0f,
        0.5f, 0.5f, 0f,
        0.5f, 0.5f, 0f,
        0.5f, -0.5f, 0f,
    };

    int[] indices =
    {
        0, 1, 3,
        3, 1, 2,

        4, 5, 7,
        7, 5, 6
    };

    float[] colors =
    {
        0f, 0f, 1f, 1f,
        0f, 0f, 1f, 0.5f,
        0f, 1f, 0f, 0.1f,
        0f, 1f, 0f, 1f,

        0f, 1f, 0f, 0.1f,
        0f, 1f, 0f, 1f,
        0f, 0f, 1f, 1f,
        0f, 0f, 1f, 0.5f
    };

    public SplashScene()
    {
        mesh = new Mesh(indices, vertices, colors);
    }

    @Override
    public void update()
    {
        if (Keyboard.keyPressedOnce(Key.SPACE))
            mesh = new Mesh(indices, vertices, colors);
    }

    @Override
    public void render()
    {
        GL11.glClearColor(33f / 255f, 30f / 255f, 40f / 255f, 1f);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        if (mesh != null)
        {
            shader.bind();
            mesh.render();
            shader.unbind();
        }
    }
}
