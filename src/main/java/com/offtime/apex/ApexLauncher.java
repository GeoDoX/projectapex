package com.offtime.apex;

import com.offtime.apex.engine.Game;
import com.offtime.apex.engine.Launcher;

public class ApexLauncher extends Launcher
{
    public static void main(String[] args)
    {
        Game game = new ApexGame();

        game.start();
    }
}
