package com.offtime.apex;

import com.offtime.apex.engine.Game;
import com.offtime.apex.engine.scene.SceneManager;
import com.offtime.apex.scenes.SplashScene;

public class ApexGame extends com.offtime.apex.engine.Game
{
    private static Game instance;

    @Override
    public void initialize()
    {
        if (instance != null)
            return;

        instance = this;

        SceneManager.push(new SplashScene());
    }

    public static Game instance()
    {
        return instance;
    }
}
