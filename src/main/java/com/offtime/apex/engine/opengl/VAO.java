package com.offtime.apex.engine.opengl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.util.ArrayList;
import java.util.List;

public class VAO
{
    private static final List<VAO> vaos = new ArrayList<>(0);

    public static void disposeAll()
    {
        for (VAO vao : vaos)
        {
            vao.unbind();

            for (VBO vbo : vao.vbos)
                vbo.dispose();

            GL30.glDeleteVertexArrays(vao.vaoID);
        }

        vaos.clear();
    }

    public int vaoID;
    public VBO indices;
    public VBO[] vbos;

    public VAO(VBO indices, VBO... vbos)
    {
        this.vaoID = GL30.glGenVertexArrays();
        this.indices = indices;
        this.vbos = vbos;

        bind();

        for (int i = 0; i < vbos.length; i++)
        {
            VBO vbo = vbos[i];
            vbo.bind();
            GL20.glVertexAttribPointer(i, vbo.packetLength, GL11.GL_FLOAT, false, 0, 0);
            vbo.unbind();
        }

        unbind();

        vaos.add(this);
    }

    public void bind()
    {
        GL30.glBindVertexArray(vaoID);
    }

    public void unbind()
    {
        GL30.glBindVertexArray(0);
    }

    public void dispose()
    {
        this.unbind();
        GL30.glDeleteVertexArrays(vaoID);
        vaos.remove(this);
    }
}
