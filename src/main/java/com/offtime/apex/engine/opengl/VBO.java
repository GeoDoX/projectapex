package com.offtime.apex.engine.opengl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL15.*;

public class VBO
{
    private static final List<VBO> vbos = new ArrayList<>(0);

    public static void disposeAll()
    {
        for (VBO vbo : vbos)
        {
            vbo.unbind();
            glDeleteBuffers(vbo.vboID);
        }

        vbos.clear();
    }

    public int vboID;
    public int dataType;
    public int dataLength;
    public int packetLength;

    public VBO(float[] data, int packetLength)
    {
        this.vboID = GL15.glGenBuffers();

        this.dataType = GL11.GL_FLOAT;
        this.dataLength = data.length;
        this.packetLength = packetLength;

        bind();

        glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW);

        unbind();

        vbos.add(this);
    }

    public VBO(int[] indices)
    {
        this.vboID = glGenBuffers();
        this.dataType = GL11.GL_INT;
        this.dataLength = indices.length;

        bind();

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);

        unbind();

        vbos.add(this);
    }

    public void bind()
    {
        if (dataType == GL11.GL_FLOAT)
            glBindBuffer(GL_ARRAY_BUFFER, vboID);
        else if (dataType == GL11.GL_INT)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
    }

    public void unbind()
    {
        if (dataType == GL11.GL_FLOAT)
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        else if (dataType == GL11.GL_INT)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public void dispose()
    {
        this.unbind();
        glDeleteBuffers(vboID);
        vbos.remove(this);
    }
}
