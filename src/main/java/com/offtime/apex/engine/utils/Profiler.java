package com.offtime.apex.engine.utils;

import java.util.*;

public class Profiler
{
    private static Map<String, Task> tasks = new HashMap<>(0);
    private static List<Task> sortedTasks = new ArrayList<>(0);

    public static void create(String id)
    {
        tasks.put(id, new Task(id));
    }

    public static void begin(String id)
    {
        if (!tasks.containsKey(id))
            create(id);

        tasks.get(id).begin();
    }

    public static void end(String id)
    {
        if (!tasks.containsKey(id))
            return;

        tasks.get(id).end();
    }

    public static void refresh()
    {
        sortedTasks.clear();
        sortedTasks.addAll(tasks.values());

        sortedTasks.sort(Comparator.comparingLong(Task::diff));
        Collections.reverse(sortedTasks);
    }

    public static void print()
    {
        // Will also always be in index 0 (if updated properly)
        long total = tasks.get("profiler").diff();

        System.out.println(" - Begin Profiler - \n");

        for (Task task : sortedTasks)
        {
            long diff = task.diff();
            float percent = (float) diff / total;

            System.out.println("    [" + task.id + "] " + diff + " / " + total + " (%" + (percent * 100f) + ")");
        }

        System.out.println("\n - End Profiler - ");
    }

    private static class Task
    {
        final String id;

        private boolean running;
        private long startTime;
        private long endTime;

        public Task(String id)
        {
            this.id = id;
        }

        public void begin()
        {
            running = true;
            startTime = Time.nano();
        }

        public void end()
        {
            running = false;
            endTime = Time.nano();
        }

        public long diff()
        {
            if (running)
                return -1;

            return endTime -  startTime;
        }
    }
}
