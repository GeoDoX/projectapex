package com.offtime.apex.engine.utils;

public class Time
{
    public static double deltaTime;

    public static long nano()
    {
        return System.nanoTime();
    }

    public static long millis()
    {
        return System.currentTimeMillis();
    }
}
