package com.offtime.apex.engine.files;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public class File
{
    protected String path;

    public File(String path)
    {
        this.path = Objects.requireNonNull(path, "Null Path");
    }

    public void create() throws IOException
    {
        if (!exists())
            Files.createFile(asPath());
    }

    public void delete() throws IOException
    {
        if (exists())
            Files.delete(asPath());
    }

    public boolean exists()
    {
        return Files.exists(Paths.get(this.path));
    }

    public String path()
    {
        return path;
    }

    public long lastModified()
    {
        if (exists())
            return asFile().lastModified();

        return 0;
    }

    public Directory directory()
    {
        return new Directory(asFile().getParentFile().toPath().toString());
    }

    public InputStream inputStream() throws IOException
    {
        return Files.newInputStream(asPath());
    }

    public String readFirstLine() throws IOException
    {
        try (BufferedReader r = new BufferedReader(new InputStreamReader(inputStream())))
        {
            return r.readLine();
        }
    }

    public List<String> readAllLines() throws IOException
    {
        List<String> lines = new ArrayList<>(0);

        if (!exists())
            return lines;

        try (BufferedReader r = new BufferedReader(new InputStreamReader(inputStream())))
        {
            String line;
            while ((line = r.readLine()) != null)
                lines.add(line);
        }

        return lines;
    }

    public OutputStream outputStream() throws IOException
    {
        return Files.newOutputStream(asPath());
    }

    public void writeAllLines(List<String> lines) throws IOException
    {
        if (!exists())
            return;

        try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(outputStream())))
        {
            for (int i = 0; i < lines.size(); i++)
            {
                w.write(lines.get(i));

                if (i < lines.size() - 1)
                    w.newLine();
            }
        }
    }

    public java.io.File asFile()
    {
        return new java.io.File(this.path);
    }

    public java.nio.file.Path asPath()
    {
        return Paths.get(this.path);
    }

    public boolean isFile()
    {
        return asFile().isFile();
    }

    public boolean isDirectory()
    {
        return asFile().isDirectory();
    }
}
