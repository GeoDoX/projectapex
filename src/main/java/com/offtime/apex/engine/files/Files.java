package com.offtime.apex.engine.files;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.regex.Matcher;

/**
 * Created by GeoDoX on 2017-12-13.
 */
public class Files
{
    public static final String WORKING_DIRECTORY_PATH = Paths.get(".").toAbsolutePath().normalize().toString();

    public static File file(String base, String... additional)
    {
        return new File(path(base, additional));
    }

    public static String path(String base, String... additional)
    {
        base = Objects.requireNonNull(base, "Null Path");

        return Paths.get(base, additional).toString();
    }

    public static String relativePath(String base, String path)
    {
        base = Objects.requireNonNull(base, "Null Path");

        return Paths.get(base).relativize(Paths.get(path)).toString();
    }

    public static String absolutePath(String base, String path)
    {
        base = Objects.requireNonNull(base, "Null Path");

        return Paths.get(base).getParent().resolve(path).normalize().toString();
    }

    public static String normalizePath(String path)
    {
        path = Objects.requireNonNull(path, "Null Path");

        path = path.replaceAll("/+", Matcher.quoteReplacement(java.io.File.separator));
        path = path.replaceAll("\\+", Matcher.quoteReplacement(java.io.File.separator));

        return path;
    }

    public static OutputStream newOutputStream(String path) throws IOException
    {
        path = Objects.requireNonNull(path, "Null Path");

        return java.nio.file.Files.newOutputStream(Paths.get(path));
    }
}
