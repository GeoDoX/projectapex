package com.offtime.apex.engine.graphics;

import com.offtime.apex.engine.opengl.VAO;
import com.offtime.apex.engine.opengl.VBO;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

public class Model extends Mesh
{
    public Texture texture;

    public Model(int[] indices, float[] vertices, float[] colors, Texture texture, float[] uvCoords)
    {
        this.vao = new VAO(new VBO(indices), new VBO(vertices, 3), new VBO(colors, 4), new VBO(uvCoords, 2));
        this.texture = texture;
    }

    public Model(Mesh mesh, Texture texture, float[] uvCoords)
    {
        this.vao = new VAO(mesh.vao.indices, mesh.vao.vbos[0], mesh.vao.vbos[1], new VBO(uvCoords, 2));
        this.texture = texture;
    }

    @Override
    public void bind()
    {
        texture.bind();
        super.bind();
    }

    @Override
    public void unbind()
    {
        super.unbind();
        texture.unbind();
    }

    @Override
    public void render()
    {
        bind();
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        enableVertexAttribArrays(0, 1, 2);
        GL11.glDrawElements(GL11.GL_TRIANGLES, vao.indices.dataLength, GL11.GL_UNSIGNED_INT, 0);
        disableVertexAttribArrays(0, 1, 2);
        unbind();
    }
}
