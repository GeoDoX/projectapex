package com.offtime.apex.engine.graphics;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL20;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ShaderProgram
{
    private static List<ShaderProgram> programs = new ArrayList<>(0);

    public static void disposeAll()
    {
        for (ShaderProgram program : programs)
        {
            program.unbind();

            for (Shader shader : program.shaders.values())
                GL20.glDetachShader(program.glProgramID, shader.glShaderID);

            GL20.glDeleteProgram(program.glProgramID);
        }

        programs.clear();
    }

    public final int glProgramID;
    public Map<Integer, Shader> shaders;

    public ShaderProgram(Shader... shaders)
    {
        this.glProgramID = GL20.glCreateProgram();
        this.shaders = new HashMap<>(shaders.length);

        for (Shader shader : shaders)
        {
            this.shaders.put(shader.glShaderID, shader);

            GL20.glAttachShader(this.glProgramID, shader.glShaderID);
        }

        this.bindAttributeLocations();

        GL20.glLinkProgram(this.glProgramID);
        GL20.glValidateProgram(this.glProgramID);

        this.getUniformLocations();

        programs.add(this);
    }

    public void bindAttributeLocations() {}
    public void getUniformLocations() {}

    public void bind()
    {
        GL20.glUseProgram(this.glProgramID);
    }

    public void unbind()
    {
        GL20.glUseProgram(0);
    }

    public Shader shader(int glShaderType)
    {
        return this.shaders.get(glShaderType);
    }

    public void bindAttribute(int index, String identifier)
    {
        GL20.glBindAttribLocation(this.glProgramID, index, identifier);
    }

    public void getUniformLocation(String identifier)
    {
        GL20.glGetUniformLocation(this.glProgramID, identifier);
    }

    public void putFloat(int location, float value)
    {
        GL20.glUniform1f(location, value);
    }

    public void putInt(int location, int value)
    {
        GL20.glUniform1i(location, value);
    }

    public void putBoolean(int location, boolean value)
    {
        if (value)
            putInt(location, 1);
        else
            putInt(location, 0);
    }

    public void putVector3f(int location, Vector3f vector)
    {
        GL20.glUniform3f(location, vector.x, vector.y, vector.z);
    }

    public void putMatrix4f(int location, Matrix4f matrix)
    {
        try (MemoryStack stack = MemoryStack.stackPush())
        {
            FloatBuffer buffer = stack.mallocFloat(16);
            GL20.glUniformMatrix4fv(location, false, matrix.get(buffer));
        }
    }

    public void dispose()
    {
        this.unbind();

        for (Shader shader : this.shaders.values())
            GL20.glDetachShader(this.glProgramID, shader.glShaderID);

        GL20.glDeleteProgram(this.glProgramID);

        programs.remove(this);
    }
}
