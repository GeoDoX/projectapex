package com.offtime.apex.engine.graphics;

import com.offtime.apex.engine.files.File;
import org.lwjgl.opengl.GL11;
import org.lwjgl.stb.STBImage;
import org.lwjgl.system.MemoryUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class Texture
{
    private static List<Texture> textures = new ArrayList<>(0);

    public static void disposeAll()
    {
        for (Texture texture : textures)
        {
            texture.unbind();
            GL11.glDeleteTextures(texture.glTextureID);
        }

        textures.clear();
    }

    public int glTextureID;
    public int width, height;
    public boolean containsTransparentPixels;

    public Texture(File file)
    {
        ByteBuffer imageBytes;

        IntBuffer widthBuffer = null;
        IntBuffer heightBuffer = null;
        IntBuffer channelsBuffer = null;

        try
        {
            widthBuffer = MemoryUtil.memAllocInt(1);
            heightBuffer = MemoryUtil.memAllocInt(1);
            channelsBuffer = MemoryUtil.memAllocInt(1);

            imageBytes = STBImage.stbi_load_from_memory(loadImage(file), widthBuffer, heightBuffer, channelsBuffer, 0);
        }
        finally
        {
            if (widthBuffer != null)
                MemoryUtil.memFree(widthBuffer);

            if (heightBuffer != null)
                MemoryUtil.memFree(heightBuffer);

            if (channelsBuffer != null)
                MemoryUtil.memFree(channelsBuffer);
        }

        if (imageBytes == null)
            throw new IllegalStateException("Failed to load Image from '" + file.path() + "'");

        this.glTextureID = GL11.glGenTextures();
        this.width = widthBuffer.get();
        this.height = heightBuffer.get();
        this.containsTransparentPixels = channelsBuffer.get() == 3;

        this.bind();

        if (containsTransparentPixels)
        {
            GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageBytes);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        }
        else
            GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width, height, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, imageBytes);

        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

        MemoryUtil.memFree(imageBytes);

        this.unbind();

        textures.add(this);
    }

    public void bind()
    {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, glTextureID);
    }

    public void unbind()
    {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
    }

    public void render(int x,int y,int w,int h)
    {
        this.bind();

        GL11.glBegin(GL11.GL_TRIANGLES);
        GL11.glColor4f( 1F,1F,1F,1F );

        GL11.glTexCoord2f( 0F,0F );
        GL11.glVertex2i( x,y );
        GL11.glTexCoord2f( 0F,1F );
        GL11.glVertex2i( x,y+h );
        GL11.glTexCoord2f( 1F,1F );
        GL11.glVertex2i( x+w,y+h );
        GL11.glTexCoord2f( 1F,0F );
        GL11.glVertex2i( x+w,y );

        GL11.glEnd();

        this.unbind();
    }

    public void dispose()
    {
        GL11.glDeleteTextures(glTextureID);
        textures.remove(this);
    }

    private static ByteBuffer loadImage(File file)
    {
        ByteBuffer imageBytes = null;

        try (FileInputStream is = new FileInputStream(file.asFile()))
        {
            FileChannel channel = is.getChannel();
            imageBytes = MemoryUtil.memAlloc((int) (channel.size() + 1));

            //noinspection StatementWithEmptyBody
            while (channel.read(imageBytes) != -1);

            imageBytes.flip();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return imageBytes;
    }
}
