package com.offtime.apex.engine.graphics;

import com.offtime.apex.engine.files.File;
import org.lwjgl.opengl.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Shader
{
    private static List<Shader> shaders = new ArrayList<>(0);

    public static void disposeAll()
    {
        for (Shader shader : shaders)
            GL20.glDeleteShader(shader.glShaderID);

        shaders.clear();
    }

    public final int glShaderID;
    public final int glShaderType;

    public Shader(int glShaderType, File file)
    {
        this.glShaderID = GL20.glCreateShader(glShaderType);
        this.glShaderType = glShaderType;

        GL20.glShaderSource(this.glShaderID, loadSource(glShaderType, file));
        GL20.glCompileShader(this.glShaderID);

        if (GL20.glGetShaderi(this.glShaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
        {
            System.out.println("Failed to compile " + shaderType(this.glShaderType) + " shader from '" + file.path() + "'");
            System.out.println(GL20.glGetShaderInfoLog(this.glShaderID));
        }

        shaders.add(this);
    }

    public void dispose()
    {
        GL20.glDeleteShader(this.glShaderID);
        shaders.remove(this);
    }

    public static String loadSource(int glShaderType, File file)
    {
        StringBuilder sourceBuilder = new StringBuilder();

        try
        {
            List<String> lines = file.readAllLines();

            for (String line : lines)
                sourceBuilder.append(line).append("//\n");
        }
        catch (IOException e)
        {
            System.out.println("Failed to load " + shaderType(glShaderType) + " shader from '" + file.path() + "'");
            e.printStackTrace();
        }

        return sourceBuilder.toString();
    }

    public static String shaderType(int glShaderType)
    {
        String shaderType;

        switch (glShaderType)
        {
            case GL20.GL_VERTEX_SHADER:
                shaderType = "vertex";
                break;
            case GL20.GL_FRAGMENT_SHADER:
                shaderType = "fragment";
                break;
            case GL32.GL_GEOMETRY_SHADER:
                shaderType = "geometry";
                break;
            case GL40.GL_TESS_CONTROL_SHADER:
                shaderType = "tessellation control";
                break;
            case GL40.GL_TESS_EVALUATION_SHADER:
                shaderType = "tessellation evaluation";
                break;
            case GL43.GL_COMPUTE_SHADER:
                shaderType = "compute";
                break;
            default:
                shaderType = "unknown";
        }

        return shaderType;
    }
}
