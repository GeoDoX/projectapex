package com.offtime.apex.engine.graphics;

import com.offtime.apex.engine.opengl.VAO;
import com.offtime.apex.engine.opengl.VBO;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class Mesh
{
    public VAO vao;

    Mesh() {}

    public Mesh(int[] indices, float[] vertices, float[] colors)
    {
        if (colors == null)
            colors = defaultColorData(indices.length);

        this.vao = new VAO(new VBO(indices), new VBO(vertices, 3), new VBO(colors, 4));
    }

    public void bind()
    {
        vao.bind();
        vao.indices.bind();
    }

    public void unbind()
    {
        vao.indices.unbind();
        vao.unbind();
    }

    public void render()
    {
        bind();
        enableVertexAttribArrays(0, 1);
        GL11.glDrawElements(GL11.GL_TRIANGLES, vao.indices.dataLength, GL11.GL_UNSIGNED_INT, 0);
        disableVertexAttribArrays(0, 1);
        unbind();
    }

    public void dispose()
    {
        vao.dispose();
    }

    public static void enableVertexAttribArrays(int... indexes)
    {
        for (int index : indexes)
            GL20.glEnableVertexAttribArray(index);
    }

    public static void disableVertexAttribArrays(int... indexes)
    {
        for (int index : indexes)
            GL20.glDisableVertexAttribArray(index);
    }

    private static float[] defaultColorData(int length)
    {
        float[] colors = new float[length * 4]; // r, g, b, a

        for (int i = 0; i < colors.length; i++)
        {
            if (i % 4 == 3)
                colors[i] = 1f;
            else
                colors[i] = 0f;
        }

        return colors;
    }
}
