package com.offtime.apex.engine.graphics;

import com.offtime.apex.engine.files.File;
import org.lwjgl.opengl.GL20;

public class FragmentShader extends Shader
{
    public FragmentShader(File file)
    {
        super(GL20.GL_FRAGMENT_SHADER, file);
    }
}
