package com.offtime.apex.engine.graphics;

import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Camera
{
    public Vector3f position;
    public Vector3f rotation;
    public Matrix4f viewMatrix;
    public Frustum viewFrustum;

    public Camera(Frustum viewFrustum)
    {
        this(viewFrustum, new Vector3f(), new Vector3f());
    }

    public Camera(Frustum viewFrustum, Vector3f position, Vector3f rotation)
    {
        this.viewFrustum = viewFrustum;
        this.position = position;
        this.rotation = rotation;

        this.viewMatrix = new Matrix4f();
        this.recalculateMatrix();
    }

    public void translate(float dx, float dy, float dz)
    {
        this.position.add(dx, dy, dz);
        this.recalculateMatrix();
    }

    public void rotate(float drx, float dry, float drz)
    {
        this.rotation.add(drx, dry, drz);
        this.recalculateMatrix();
    }

    public void recalculateMatrix()
    {
        this.viewMatrix.identity();
        this.viewMatrix.translate(-this.position.x, -this.position.y, -this.position.z);
        this.viewMatrix.rotateXYZ((float) Math.toRadians(this.rotation.x), (float) Math.toRadians(this.rotation.y), (float) Math.toRadians(this.rotation.z));
    }

    public static class Frustum
    {
        public float width, height;
        public float fov;
        public float near, far;

        public boolean orthographic;

        public Matrix4f matrix;
        public FrustumIntersection intersection;

        private Frustum(float width, float height, float fovRad, float near, float far, boolean orthographic)
        {
            this.width = width;
            this.height = height;
            this.fov = fovRad;
            this.near = near;
            this.far = far;

            this.orthographic = orthographic;

            this.matrix = new Matrix4f();
            this.recalculateMatrix();
        }

        public void recalculateMatrix()
        {
            this.matrix.identity();

            if (this.orthographic)
                this.matrix.ortho(0, this.width, -this.height, 0, this.near, this.far);
            else
                this.matrix.perspective(fov, this.width / this.height, this.near, this.far);

            intersection.set(this.matrix, false);
        }

        public static Frustum orthographic(float width, float height, float near, float far)
        {
            return new Frustum(width, height, 0, near, far, true);
        }

        public static Frustum perspective(float width, float height, float fov, float near, float far)
        {
            return new Frustum(width, height, fov, near, far, false);
        }
    }
}
