package com.offtime.apex.engine.graphics;

import com.offtime.apex.engine.files.File;
import org.lwjgl.opengl.GL20;

public class VertexShader extends Shader
{
    public VertexShader(File file)
    {
        super(GL20.GL_VERTEX_SHADER, file);
    }
}
