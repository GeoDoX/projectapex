package com.offtime.apex.engine.input;

import org.lwjgl.glfw.GLFW;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class Button
{
    private static final Map<Integer, Button> map = new HashMap<>(0);

    public final int code;

    public Button(int code)
    {
        this.code = code;

        map.put(code, this);
    }

    public static Button fromButtonCode(int button)
    {
        return map.get(button);
    }

    public static final Button PRIMARY = new Button(GLFW.GLFW_MOUSE_BUTTON_LEFT);
    public static final Button MIDDLE = new Button(GLFW.GLFW_MOUSE_BUTTON_MIDDLE);
    public static final Button SECONDARY = new Button(GLFW.GLFW_MOUSE_BUTTON_RIGHT);

    public static final Button EXTRA_1 = new Button(GLFW.GLFW_MOUSE_BUTTON_4);
    public static final Button EXTRA_2 = new Button(GLFW.GLFW_MOUSE_BUTTON_5);
    public static final Button EXTRA_3 = new Button(GLFW.GLFW_MOUSE_BUTTON_6);
    public static final Button EXTRA_4 = new Button(GLFW.GLFW_MOUSE_BUTTON_7);
    public static final Button EXTRA_5 = new Button(GLFW.GLFW_MOUSE_BUTTON_8);
}
