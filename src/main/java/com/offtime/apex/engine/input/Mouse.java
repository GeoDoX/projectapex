package com.offtime.apex.engine.input;

import org.lwjgl.glfw.*;

import java.util.HashSet;
import java.util.Set;

public class Mouse
{
    /* position of the cursor, -1 if not over window */
    private static double x, y;

    private static Set<Button> buttonsPressedLast = new HashSet<>(0);
    private static Set<Button> buttonsPressed = new HashSet<>(0);
    private static Set<Button> buttonsReleased = new HashSet<>(0);

    public static double x()
    {
        return x;
    }

    public static double y()
    {
        return y;
    }

    public static boolean buttonPressedOnce(Button button)
    {
        return buttonsPressed.contains(button) && !buttonsPressedLast.contains(button);
    }

    public static boolean buttonPressed(Button button)
    {
        return buttonsPressed.contains(button);
    }

    public static boolean buttonReleased(Button button)
    {
        return buttonsReleased.contains(button);
    }

    public static void update()
    {
        buttonsPressedLast.clear();
        buttonsPressedLast.addAll(buttonsPressed);

        buttonsReleased.clear();
    }

    public static class CursorEnterCallback extends GLFWCursorEnterCallback
    {
        @Override
        public void invoke(long window, boolean entered)
        {
            x = entered ? x : -1;
            y = entered ? y : -1;
        }
    }

    public static class CursorPositionCallback extends GLFWCursorPosCallback
    {
        @Override
        public void invoke(long window, double posX, double posY)
        {
            x = posX;
            y = posY;
        }
    }

    public static class ButtonCallback extends GLFWMouseButtonCallback
    {
        @Override
        public void invoke(long window, int button, int action, int mods)
        {
            Button b = Button.fromButtonCode(button);

            if (action == GLFW.GLFW_PRESS)
                buttonsPressed.add(b);
            else if (action == GLFW.GLFW_RELEASE)
            {
                buttonsPressed.remove(b);
                buttonsReleased.add(b);
            }
        }
    }
}
