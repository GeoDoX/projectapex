package com.offtime.apex.engine.input;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;

import java.util.HashSet;
import java.util.Set;

public class Keyboard
{
    private static Set<Key> keysPressedLast = new HashSet<>(0);
    private static Set<Key> keysPressed = new HashSet<>(0);
    private static Set<Key> keysReleased = new HashSet<>(0);

    public static boolean keyPressedOnce(Key key)
    {
        return keysPressed.contains(key) && !keysPressedLast.contains(key);
    }

    public static boolean keyPressed(Key key)
    {
        return keysPressed.contains(key);
    }

    public static boolean keyReleased(Key key)
    {
        return keysReleased.contains(key);
    }

    public static void update()
    {
        keysPressedLast.clear();
        keysPressedLast.addAll(keysPressed);

        keysReleased.clear();
    }

    public static class Callback extends GLFWKeyCallback
    {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods)
        {
            Key k = Key.fromKeyCode(key);

            if (action == GLFW.GLFW_PRESS)
                keysPressed.add(k);
            else if (action == GLFW.GLFW_RELEASE)
            {
                keysPressed.remove(k);
                keysReleased.add(k);
            }
        }
    }
}
