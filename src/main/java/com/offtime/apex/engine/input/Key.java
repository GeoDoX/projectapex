package com.offtime.apex.engine.input;

import org.lwjgl.glfw.GLFW;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class Key
{
    private static final Map<Integer, Key> map = new HashMap<>(101); // 101 Keys by default

    public final int code;

    public Key(int code)
    {
        this.code = code;

        map.put(code, this);
    }

    public static Key fromKeyCode(int keycode)
    {
        return map.get(keycode);
    }

    public static final Key ZERO  = new Key(GLFW.GLFW_KEY_0);
    public static final Key ONE   = new Key(GLFW.GLFW_KEY_1);
    public static final Key TWO   = new Key(GLFW.GLFW_KEY_2);
    public static final Key THREE = new Key(GLFW.GLFW_KEY_3);
    public static final Key FOUR  = new Key(GLFW.GLFW_KEY_4);
    public static final Key FIVE  = new Key(GLFW.GLFW_KEY_5);
    public static final Key SIX   = new Key(GLFW.GLFW_KEY_6);
    public static final Key SEVEN = new Key(GLFW.GLFW_KEY_7);
    public static final Key EIGHT = new Key(GLFW.GLFW_KEY_8);
    public static final Key NINE  = new Key(GLFW.GLFW_KEY_9);

    public static final Key A = new Key(GLFW.GLFW_KEY_A);
    public static final Key B = new Key(GLFW.GLFW_KEY_B);
    public static final Key C = new Key(GLFW.GLFW_KEY_C);
    public static final Key D = new Key(GLFW.GLFW_KEY_D);
    public static final Key E = new Key(GLFW.GLFW_KEY_E);
    public static final Key F = new Key(GLFW.GLFW_KEY_F);
    public static final Key G = new Key(GLFW.GLFW_KEY_G);
    public static final Key H = new Key(GLFW.GLFW_KEY_H);
    public static final Key I = new Key(GLFW.GLFW_KEY_I);
    public static final Key J = new Key(GLFW.GLFW_KEY_J);
    public static final Key K = new Key(GLFW.GLFW_KEY_K);
    public static final Key L = new Key(GLFW.GLFW_KEY_L);
    public static final Key M = new Key(GLFW.GLFW_KEY_M);
    public static final Key N = new Key(GLFW.GLFW_KEY_N);
    public static final Key O = new Key(GLFW.GLFW_KEY_O);
    public static final Key P = new Key(GLFW.GLFW_KEY_P);
    public static final Key Q = new Key(GLFW.GLFW_KEY_Q);
    public static final Key R = new Key(GLFW.GLFW_KEY_R);
    public static final Key S = new Key(GLFW.GLFW_KEY_S);
    public static final Key T = new Key(GLFW.GLFW_KEY_T);
    public static final Key U = new Key(GLFW.GLFW_KEY_U);
    public static final Key V = new Key(GLFW.GLFW_KEY_V);
    public static final Key W = new Key(GLFW.GLFW_KEY_W);
    public static final Key X = new Key(GLFW.GLFW_KEY_X);
    public static final Key Y = new Key(GLFW.GLFW_KEY_Y);
    public static final Key Z = new Key(GLFW.GLFW_KEY_Z);

    public static final Key F1  = new Key(GLFW.GLFW_KEY_F1);
    public static final Key F2  = new Key(GLFW.GLFW_KEY_F2);
    public static final Key F3  = new Key(GLFW.GLFW_KEY_F3);
    public static final Key F4  = new Key(GLFW.GLFW_KEY_F4);
    public static final Key F5  = new Key(GLFW.GLFW_KEY_F5);
    public static final Key F6  = new Key(GLFW.GLFW_KEY_F6);
    public static final Key F7  = new Key(GLFW.GLFW_KEY_F7);
    public static final Key F8  = new Key(GLFW.GLFW_KEY_F8);
    public static final Key F9  = new Key(GLFW.GLFW_KEY_F9);
    public static final Key F10 = new Key(GLFW.GLFW_KEY_F10);
    public static final Key F11 = new Key(GLFW.GLFW_KEY_F11);
    public static final Key F12 = new Key(GLFW.GLFW_KEY_F12);

    public static final Key ESCAPE        = new Key(GLFW.GLFW_KEY_ESCAPE);
    public static final Key TILDE         = new Key(GLFW.GLFW_KEY_GRAVE_ACCENT);
    public static final Key TAB           = new Key(GLFW.GLFW_KEY_TAB);
    public static final Key CAPS_LOCK     = new Key(GLFW.GLFW_KEY_CAPS_LOCK);
    public static final Key SHIFT_LEFT    = new Key(GLFW.GLFW_KEY_LEFT_SHIFT);
    public static final Key CONTROL_LEFT  = new Key(GLFW.GLFW_KEY_LEFT_CONTROL);
    public static final Key ALT_LEFT      = new Key(GLFW.GLFW_KEY_LEFT_ALT);
    public static final Key SPACE         = new Key(GLFW.GLFW_KEY_SPACE);
    public static final Key ALT_RIGHT     = new Key(GLFW.GLFW_KEY_RIGHT_ALT);
    public static final Key MENU          = new Key(GLFW.GLFW_KEY_MENU);
    public static final Key CONTROL_RIGHT = new Key(GLFW.GLFW_KEY_RIGHT_CONTROL);
    public static final Key SHIFT_RIGHT   = new Key(GLFW.GLFW_KEY_RIGHT_SHIFT);
    public static final Key ENTER         = new Key(GLFW.GLFW_KEY_ENTER);
    public static final Key BACKSPACE     = new Key(GLFW.GLFW_KEY_BACKSPACE);

    public static final Key APOSTROPHE    = new Key(GLFW.GLFW_KEY_APOSTROPHE);
    public static final Key COMMA         = new Key(GLFW.GLFW_KEY_COMMA);
    public static final Key MINUS         = new Key(GLFW.GLFW_KEY_MINUS);
    public static final Key PERIOD        = new Key(GLFW.GLFW_KEY_PERIOD);
    public static final Key SLASH         = new Key(GLFW.GLFW_KEY_SLASH);
    public static final Key SEMICOLON     = new Key(GLFW.GLFW_KEY_SEMICOLON);
    public static final Key EQUAL         = new Key(GLFW.GLFW_KEY_EQUAL);
    public static final Key LEFT_BRACKET  = new Key(GLFW.GLFW_KEY_LEFT_BRACKET);
    public static final Key RIGHT_BRACKET = new Key(GLFW.GLFW_KEY_RIGHT_BRACKET);

    public static final Key INSERT       = new Key(GLFW.GLFW_KEY_INSERT);
    public static final Key DELETE       = new Key(GLFW.GLFW_KEY_DELETE);
    public static final Key HOME         = new Key(GLFW.GLFW_KEY_HOME);
    public static final Key END          = new Key(GLFW.GLFW_KEY_END);
    public static final Key PAGE_UP      = new Key(GLFW.GLFW_KEY_PAGE_UP);
    public static final Key PAGE_DOWN    = new Key(GLFW.GLFW_KEY_PAGE_DOWN);
    public static final Key SCROLL_LOCK  = new Key(GLFW.GLFW_KEY_SCROLL_LOCK);
    public static final Key PRINT_SCREEN = new Key(GLFW.GLFW_KEY_PRINT_SCREEN);
    public static final Key PAUSE        = new Key(GLFW.GLFW_KEY_PAUSE);
    public static final Key UP           = new Key(GLFW.GLFW_KEY_UP);
    public static final Key LEFT         = new Key(GLFW.GLFW_KEY_LEFT);
    public static final Key DOWN         = new Key(GLFW.GLFW_KEY_DOWN);
    public static final Key RIGHT        = new Key(GLFW.GLFW_KEY_RIGHT);

    public static final Key NP_1 = new Key(GLFW.GLFW_KEY_KP_1);
    public static final Key NP_2 = new Key(GLFW.GLFW_KEY_KP_2);
    public static final Key NP_3 = new Key(GLFW.GLFW_KEY_KP_3);
    public static final Key NP_4 = new Key(GLFW.GLFW_KEY_KP_4);
    public static final Key NP_5 = new Key(GLFW.GLFW_KEY_KP_5);
    public static final Key NP_6 = new Key(GLFW.GLFW_KEY_KP_6);
    public static final Key NP_7 = new Key(GLFW.GLFW_KEY_KP_7);
    public static final Key NP_8 = new Key(GLFW.GLFW_KEY_KP_8);
    public static final Key NP_9 = new Key(GLFW.GLFW_KEY_KP_9);
    public static final Key NP_0 = new Key(GLFW.GLFW_KEY_KP_0);

    public static final Key NP_NUM_LOCK  = new Key(GLFW.GLFW_KEY_NUM_LOCK);
    public static final Key NP_DIVIDE    = new Key(GLFW.GLFW_KEY_KP_DIVIDE);
    public static final Key NP_MULTIPLY  = new Key(GLFW.GLFW_KEY_KP_MULTIPLY);
    public static final Key NP_SUBTRACT = new Key(GLFW.GLFW_KEY_KP_SUBTRACT);
    public static final Key NP_ADD       = new Key(GLFW.GLFW_KEY_KP_ADD);
    public static final Key NP_ENTER     = new Key(GLFW.GLFW_KEY_ENTER);
    public static final Key NP_COMMA     = new Key(GLFW.GLFW_KEY_COMMA);
}
