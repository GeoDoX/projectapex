package com.offtime.apex.engine;

import com.offtime.apex.engine.graphics.Shader;
import com.offtime.apex.engine.graphics.ShaderProgram;
import com.offtime.apex.engine.graphics.Texture;
import com.offtime.apex.engine.input.Keyboard;
import com.offtime.apex.engine.input.Mouse;
import com.offtime.apex.engine.opengl.VAO;
import com.offtime.apex.engine.opengl.VBO;
import com.offtime.apex.engine.scene.SceneManager;
import com.offtime.apex.engine.utils.Profiler;
import com.offtime.apex.engine.utils.Time;
import com.offtime.apex.engine.window.Window;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;

public abstract class Game
{
    private Properties properties;

    private Window window;

    public Game()
    {
        this(new Properties());
    }

    public Game(Properties properties)
    {
        this.properties = properties;

        init();
    }

    private void init()
    {
        /* Create the GLFW Error Callback */
        GLFWErrorCallback.createPrint(System.err).set();

        if (!glfwInit())
            throw new IllegalStateException("Failed to initialize GLFW");

        /* Set the default window hints */
        glfwDefaultWindowHints();

        /* Make all windows hidden and resizable by default */
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        /* Create the window and show it */
        window = Window.create(properties.title, properties.width, properties.height);

        if (properties.centered)
            window.center();

        window.show();

        setContext(window);

        /* Create the Capabilities for OpenGL */
        GL.createCapabilities(); // CONSIDER: Setting the forwardCompatible to true (Enables support for Mac?)
    }

    public final void start()
    {
        initialize();

        /* Game Loop */
        final long TIME_PER_UPDATE = (long) (1e9 / properties.UPS);
        final long TIME_PER_RENDER = (long) (1e9 / properties.FPS);

        short currentUpdates = 0;
        short currentRenders = 0;
        short maxUpdates = 20;
        short maxRenders = 5;

        short frames = 0;
        short updates = 0;

        long secondTimer = Time.millis();

        long nowTime;
        long elapsedTime; // Nano Seconds
        long lastTime = Time.nano();

        long dUAccumulator = 0;
        long dFAccumulator = 0;

        while (!window.shouldClose())
        {
            nowTime = Time.nano();
            elapsedTime = nowTime - lastTime;
            Time.deltaTime = elapsedTime / 1e9;
            lastTime = nowTime;

            dUAccumulator += elapsedTime;
            dFAccumulator += elapsedTime;

            Profiler.begin("profiler");

            while (dUAccumulator >= TIME_PER_UPDATE)
            {
                if (currentUpdates > maxUpdates)
                {
                    currentUpdates = 0;
                    break;
                }

                dUAccumulator -= TIME_PER_UPDATE;

                update();

                updates++;
                currentUpdates++;
            }

            while (dFAccumulator >= TIME_PER_RENDER)
            {
                if (currentRenders > maxRenders)
                {
                    currentRenders = 0;
                    break;
                }

                dFAccumulator -= TIME_PER_RENDER;

                render();

                frames++;
                currentRenders++;
            }

            Profiler.end("profiler");

            /* A second has passed, reset frames */
            if (Time.millis() - secondTimer >= 1000)
            {
                if (properties.printFPS_UPS)
                    System.out.println("Frames: " + frames + ", Updates: " + updates);

                Profiler.refresh();

                if (properties.printProfiler)
                    Profiler.print();

                updates = 0;
                frames = 0;

                secondTimer += 1000;
            }
        }

        /* Dispose of the window */
        window.dispose();
        SceneManager.dispose();
        VAO.disposeAll();
        VBO.disposeAll();
        ShaderProgram.disposeAll();
        Shader.disposeAll();
        Texture.disposeAll();

        /* Terminate GLFW and release the remaining resources */
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    public abstract void initialize();

    private void update()
    {
        glfwPollEvents();

        SceneManager.update();

        Mouse.update();
        Keyboard.update();
    }

    private void render()
    {
        SceneManager.render();

        window.draw();
    }

    public Window window()
    {
        return window;
    }

    /* CONSIDER: Do GL Capabilities work across multiple windows? */
    public void setContext(Window window)
    {
        glfwMakeContextCurrent(window.handle());
    }

    public static class Properties
    {
        public String title = "Apex Game";
        public String version = "1.0.0";

        public boolean printProfiler = false;
        public boolean printFPS_UPS = false;
        public double UPS = 120;
        public double FPS = 60;

        public int width = 1200;
        public int height = 720;

        public boolean fullscreen = false;
        public boolean centered = true;
    }
}
