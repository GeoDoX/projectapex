package com.offtime.apex.engine.window;

import static org.lwjgl.glfw.GLFW.*;

import com.offtime.apex.engine.input.Keyboard;
import com.offtime.apex.engine.input.Mouse;
import org.lwjgl.glfw.*;
import org.lwjgl.system.MemoryUtil;

public class Window
{
    private long handle;

    private int[] xPos = new int[1];
    private int[] yPos = new int[1];
    private int[] width = new int[1];
    private int[] height = new int[1];

    private GLFWKeyCallback keyCallback;
    private GLFWMouseButtonCallback mouseButtonCallback;
    private GLFWCursorPosCallback mousePositionCallback;
    private GLFWCursorEnterCallback mouseEnterCallback;

    private Window() {}

    public static Window create(String title, int width, int height)
    {
        return create(title, width, height, MemoryUtil.NULL);
    }

    public static Window create(String title, int width, int height, long share)
    {
        return create(title, 0, 0, width, height, share);
    }

    public static Window create(String title, int x, int y, int width, int height)
    {
        return create(title, x, y, width, height, MemoryUtil.NULL);
    }

    public static Window create(String title, int x, int y, int width, int height, long share)
    {
        Window window = new Window();

        window.handle = glfwCreateWindow(width, height, title, MemoryUtil.NULL, share);

        if (window.handle == MemoryUtil.NULL)
            throw new IllegalStateException("Failed to create GLFW Window");

        window.position(x, y);

        window.keyCallback = new Keyboard.Callback();
        glfwSetKeyCallback(window.handle(), window.keyCallback);

        window.mousePositionCallback = new Mouse.CursorPositionCallback();
        glfwSetCursorPosCallback(window.handle(), window.mousePositionCallback);

        window.mouseButtonCallback = new Mouse.ButtonCallback();
        glfwSetMouseButtonCallback(window.handle(), window.mouseButtonCallback);

        window.mouseEnterCallback = new Mouse.CursorEnterCallback();
        glfwSetCursorEnterCallback(window.handle(), window.mouseEnterCallback);

        return window;
    }

    public void show()
    {
        glfwShowWindow(this.handle());
    }

    public void hide()
    {
        glfwHideWindow(this.handle());
    }

    public boolean shouldClose()
    {
        return glfwWindowShouldClose(this.handle());
    }

    public void draw()
    {
        glfwSwapBuffers(this.handle());
    }

    public void close()
    {
        glfwSetWindowShouldClose(this.handle(), true);
    }

    public void dispose()
    {
        Callbacks.glfwFreeCallbacks(this.handle());
        glfwDestroyWindow(this.handle());
    }

    public void title(String title)
    {
        glfwSetWindowTitle(this.handle(), title);
    }

    public int x()
    {
        glfwGetWindowPos(this.handle(), xPos, null);

        return xPos[0];
    }

    public int y()
    {
        glfwGetWindowPos(this.handle(), null, yPos);

        return yPos[0];
    }

    public int width()
    {
        glfwGetWindowSize(this.handle(), width, null);

        return width[0];
    }

    public int height()
    {
        glfwGetWindowSize(this.handle(), null, height);

        return height[0];
    }

    public boolean visible()
    {
        return glfwGetWindowAttrib(this.handle(), GLFW_VISIBLE) == GLFW_TRUE;
    }

    public void position(int x, int y)
    {
        glfwSetWindowPos(this.handle(), x, y);
    }

    public void center()
    {
        GLFWVidMode vm = glfwGetVideoMode(glfwGetPrimaryMonitor());
        this.position((vm.width() - this.width()) / 2, (vm.height() - this.height()) / 2);
    }

    public long handle()
    {
        if (handle == 0)
            throw new RuntimeException("Window is not initialized");

        return handle;
    }
}
