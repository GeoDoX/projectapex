package com.offtime.apex.engine.scene;

public abstract class Scene
{
    public abstract void update();
    public abstract void render();

    public void dispose() {}

    public void onEnter() {}
    public void onLeave() {}
}
