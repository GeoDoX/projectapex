package com.offtime.apex.engine.scene;

import java.util.ArrayList;
import java.util.List;

public class SceneManager
{
    private static List<Scene> scenes = new ArrayList<>(0);

    public static void push(Scene scene)
    {
        scenes.add(scene);

        scene.onEnter();
    }

    public static Scene pop()
    {
        Scene s = peek();
        scenes.remove(s);

        s.onLeave();

        return s;
    }

    public static Scene peek()
    {
        return scenes.get(scenes.size() - 1);
    }

    public static void update()
    {
        if (scenes.isEmpty())
            return;

        Scene s = peek();

        s.update();
    }

    public static void render()
    {
        if (scenes.isEmpty())
            return;

        Scene s = peek();

        s.render();
    }

    public static void dispose()
    {
        while (!scenes.isEmpty())
        {
            Scene s = pop();

            s.dispose();
        }
    }
}
